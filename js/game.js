var game = new Phaser.Game(600, 800, Phaser.AUTO, '', { preload: preload, create: create, update: update, render: render });

var player;
var background;

var ACCLERATION = 600;
var DRAG = 400;
var MAXSPEED = 400;

//input
var cursors;
var fireButton;

//ilusion gerak
var bank;

//partikel ekor
var tailPlayer;

//peluru
var bullets;
var bulletTime = 0;

//Enemy
var enemies;

function preload() {
    game.load.image('background', 'assets/Background.png');
    game.load.image('ship', 'assets/Pesawat.png');
    game.load.image('bullet', 'assets/Peluru.png');
    game.load.image('enemy', 'assets/Enemy.png');
    game.load.image('explosion','assets/Explosion.png');
}

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);



    // background
    background = game.add.tileSprite(0, 0, 600, 800, 'background');

    //The Baddies
    enemies = game.add.group();
    enemies.enableBody = true;
    enemies.physicsBodyType = Phaser.Physics.ARCADE;
    enemies.createMultiple(5, 'enemy');
    enemies.setAll('anchor.x', 0.5);
    enemies.setAll('anchor.y', 0.5);
    enemies.setAll('scale.x', 0.5);
    enemies.setAll('scale.y', 0.5);
    enemies.setAll('angle', 180);
    //enemies.setAll('outBoundsKill', true); tidak berfungsi dengan baik
    //enemies.setAll('checkWorldBounds', true);

/*    enemies.forEach(function(enemy){
        addEnemyEmitterTrain(enemy);
        enemy.event.onKilled.add(function(){
            enemy.trail.kill();
        });
    });*/

    launchEnemies();

    //peluru
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(10, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('scale.x', 0.3);
    bullets.setAll('scale.y', 0.3);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);


    //the hero
    player = game.add.sprite(game.world.centerX, 700, 'ship');
    player.anchor.setTo(0.5, 0.5);
    player.scale.setTo(0.5, 0.5);
    //gerak dengan physics
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.body.maxVelocity.setTo(MAXSPEED, MAXSPEED);
    player.body.drag.setTo(DRAG, DRAG);

    //add a emitter for the ship
    tailPlayer = game.add.emitter(player.x, player.y + 10, 400);
    tailPlayer.width = 10;
    tailPlayer.makeParticles('bullet');
    tailPlayer.setXSpeed(30, -30);
    tailPlayer.setYSpeed(200, 180);
    tailPlayer.setRotation(50, -50);
    tailPlayer.setAlpha(1, 0.01, 800);
    tailPlayer.setScale(0.05, 0.4, 0.05, 0.4, 2000, Phaser.Easing.Quintic.Out);
    tailPlayer.start(false, 5000, 10);

    //init input keyboard
    cursors = game.input.keyboard.createCursorKeys();

    //fire bullet
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
}

function update() {
    // Scroll the background
    background.tilePosition.y += 2;


    //player move
    player.body.acceleration.x = 0;
    if (cursors.left.isDown) {
        player.body.acceleration.x = -ACCLERATION;
    } else if (cursors.right.isDown) {
        player.body.acceleration.x = ACCLERATION;
    }

    //firebullet
    if (fireButton.isDown || game.input.activePointer.isDown) {
        fireBullet();
    }

    //Stop at screen edges
    if (player.x > game.world.width - 50) {
        player.x = game.world.width - 50;
        player.body.acceleration.x = 0;
    }

    if (player.x < 50) {
        player.x = 50;
        player.body.acceleration.x = 0;
    }

    //Squish and rotate ship for illusion of "banking"
    bank = player.body.velocity.x / MAXSPEED;
    player.scale.x = 0.5 * (1 - Math.abs(bank) / 3);
    player.angle = bank * 10;

    // supaya tailPlayer minimili garis lurus dengan player
    tailPlayer.x = player.x;

    //Memunculkan enemy
    //launchEnemies(); // tidak rekomendasi cara ini
}

function render() {

}

function fireBullet() {
    var BULLET_SPEED = 400;
    var BULLET_TIME = 200;
    if (game.time.now > bulletTime) {
        var bullet = bullets.getFirstExists(false);

        if (bullet) {
            //bullet.reset(player.x, player.y + 8);
            //bullet.body.velocity.y = -400;
            var bulletOffset = 20 * Math.sin(game.math.degToRad(player.angle));
            bullet.reset(player.x + bulletOffset, player.y);
            bullet.angle = player.angle;
            game.physics.arcade.velocityFromAngle(bullet.angle - 90, BULLET_SPEED, bullet.body.velocity);
            bullet.body.velocity.x += player.body.velocity.x;
            bulletTime = game.time.now + BULLET_TIME;
        }
    }
}
var enemyTime = 300;

function launchEnemies() {
    var MIN_ENEMY_SPACING = 300;
    var MAX_ENEMY_SPACING = 3000;
    var ENEMY_SPEED = 300;
    

    //if (game.time.now > enemyTime) {

        var enemy = enemies.getFirstExists(false);
        if (enemy) {
            enemy.reset(game.rnd.integerInRange(0, game.width), 0.5);
            enemy.body.velocity.x = game.rnd.integerInRange(-250, 250);
            enemy.body.velocity.y = ENEMY_SPEED;
            enemy.body.drag.x = 100;

           // enemy.trail.start(false,800,1);

           // update rotation
           enemy.update = function () {
                enemy.angle = 180 - game.math.radToDeg(Math.atan2(enemy.body.velocity.x, enemy.body.velocity.y));
            }

         /*   enemy.trail.x = enemy.x;
            enemy.trail.y = enemy.y -10;

            // kill enemy once they go off screen
            if(enemy.y > game.height + 200){
                enemy.kill();
            }
            */
        }
        //var MyTime = enemyTime;
      //  enemyTime = game.time.now + 3000;
   // }
    
    //send another enemy soon
    //cara1
    //var timer = game.time.create(false);
    //timer.add(game.rnd.integerInRange(MIN_ENEMY_SPACING,MAX_ENEMY_SPACING),launchEnemies);
    //timer.start();
    //cara2
     game.time.events.add(game.rnd.integerInRange(MIN_ENEMY_SPACING,MAX_ENEMY_SPACING),launchEnemies);
}

function addEnemyEmitterTrain(enemy){
    var enemyTrail = game.add.emitter(enemy.x, (player.y - 10), 100);
    enemyTrail.width = 10;
    enemyTrail.makeParticles('explosion',[1,2,3,4,5]);
    enemyTrail.setXSpeed(20,-20);
    enemyTrail.setRotation(50,-50);
    enemyTrail.setAlpha(0.4, 0, 800);
    enemyTrail.setScale(0.01,0.1,0.01,0.1,1000, Phaser.Easing.Quintic.Out);
    enemy.trail = enemyTrail;
}
